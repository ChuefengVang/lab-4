#include "node.h"
#include <iostream>

// Take in value and create a node
node::node(int input)
{
    data=input;
    next=nullptr;
}
// Takes in an array of values and creates the appropriate nodes
node::node(int values[], int length)
{   node*temp1=new node(values[0]);
    this->data=values[0];
    temp1=this;
    for(int i = 1; i<length;i++) {
        node*temp2=new node(values[i]);
        temp2->data=values[i];
        temp1->next=temp2;
        temp1=temp2; }
}

// Default destructor
node::~node()
{
    // Hint: You don't want to just delete the current node. You need to keep track of what is next
}

// Add a value to the end node
void node::append(int input)
{   if(this==nullptr||this->next==nullptr){
       this->data=input;
    this->next==nullptr;}
    else{
        node *temp1=new node(input);
        temp1->data=input;
        temp1->next=nullptr;}
    // come back
}

// Add an array of values to the end as separate nodes
void node::append(int inputs[], int length)
{   if(this==nullptr&&length!=1){
        node *newhead=new node(inputs[1]);
        this->data=inputs[0];
        this->next=newhead;
        newhead->data=inputs[1];
        newhead->next=nullptr;
        for(int i = 2; i<length;i++){
            node *temp1=new node(inputs[i]);
            node *temp2=new node(0);
            if(i==2){
                newhead->next=temp1;
            }
            temp1->data=inputs[i];
            temp1->next=temp2;
            if(i==length-1){
            temp1->next=nullptr;
            }
            temp1=temp2;
        }
    }
    node *temp=new node(inputs[0]);
    temp->data=inputs[0];
    temp->next=nullptr;
        for (int k=1; k<length;k++) {
    node *temp2=new node(inputs[k]);
            temp2->data=inputs[k];
            temp->next=temp2;
            if(k==length-1){
                temp->next=nullptr;
            }
            temp=temp2;
        }
}

// Insert a new node after the given location
node* node::insert(int location, int value)
{ node *inserter= new node(value);
    node *check= this;
    node *check2=this;
    for(int j = 0;j<location+1;j++){
        if(check==nullptr){
            std::cout<< "Your location value is not accessible."<< std::endl;
            exit;
        }
        check=check->next;
        if(j>0){
            check2=check2->next;
        }
    }
    inserter->next=check;
    check2->next=inserter;
    return this;
    // Must return head pointer location
}

// Remove a node and link the next node to the previous node
node* node::remove(int location)
{node *holdr=this; node *holdl=this;; node *thisDelete=this;

    // Must return head pointer location
    for(int j = 0;j<location+1;j++){
        if(holdr==nullptr){
            std::cout<< "Your location value is not accessible."<< std::endl;
            exit;
        }
        holdr=holdr->next;
        if(j>1){
            holdl=holdl->next;
        }
        if(j>0){
            thisDelete=thisDelete->next;
        }
    }
    holdl->next=holdr;
    thisDelete->next=nullptr;
    thisDelete->data=0;
    delete thisDelete;
    return this;
}

// Print all nodes
void node::print()
{node *print=this;
    for(int i; print!=nullptr;print=print->next){
std::cout<<this<<std::endl; }
}

//Print the middle node
void node::print_middle()
{
    // HINT: Use a runner to traverse through the linked list at two different rates, 1 node per step
    //       and two nodes per step. When the faster one reaches the end, the slow one should be
    //       pointing to the middle
    node* runner1=this, *runner2=this;
     while(runner2->next !=NULL){
         if((runner2->next->next)== NULL) {
             runner2=runner2->next;}
         else {
             runner1=runner1->next;
             runner2=runner2->next->next;
            }}
     std::cout<<runner1->data<<std::endl;
}

// Get the value of a given node
int node::get_value(int location) {
    node *value = this;
    for (int j = 1; j <= location; j++) {
        value = value->next;
    }
    return value->data;
}
// Overwrite the value of a given node
void node::set_data(int location, int value)
{
    node *thislocation = this;
    for (int j = 1; j <= location; j++) {
        thislocation = thislocation->next;
    }
    thislocation->data= value;
}